from django.db import models


class Policy(models.Model):
    external_user_id = models.CharField(max_length=100)
    benefit = models.CharField(max_length=100)
    total_max_amount = models.PositiveIntegerField()
    currency = models.CharField(max_length=3)

    class Meta:
        unique_together = ('external_user_id', 'benefit', 'currency')
