from rest_framework import serializers
from .models import Policy


class PolicySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Policy
        fields = ['external_user_id', 'benefit', 'total_max_amount', 'currency']
