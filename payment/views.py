from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Payment
from .serializers import PaymentSerializer


@api_view(['POST'])
def submit_payment(request):
    serializer = PaymentSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save(status=Payment.ACCEPTED)
        return Response(data={
                            'authorized': True,
                            'reason': None,
                        },
                        status=status.HTTP_200_OK)
    else:
        Payment.objects.create(**serializer.data,
                               status=Payment.DENIED)
        # Get error string
        error_reason = str(serializer.errors['non_field_errors'][0])     
        return Response(data={
                            'authorized': False,
                            'reason': error_reason,
                        },
                        status=status.HTTP_200_OK)
