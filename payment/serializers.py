from rest_framework import serializers
from django.db.models import Sum

from .models import Payment
from policy.models import Policy


class PaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = ['external_user_id', 'benefit', 'currency',
                  'amount', 'timestamp']
    
    def validate(self, data):
        try:
            policy_amt = Policy.objects.get(
                            external_user_id=data['external_user_id'],
                            benefit=data['benefit'],
                            currency=data['currency']
                         ).total_max_amount
        except Policy.DoesNotExist:
            # no Policy found with this combination
            raise serializers.ValidationError('POLICY_NOT_FOUND')

        payments = Payment.objects.filter(
                       external_user_id=data['external_user_id'],
                       benefit=data['benefit'],
                       currency=data['currency'],
                       status=Payment.ACCEPTED,            
                   ).aggregate(Sum('amount'))
        
        # Check for scenarios where the payment is invalid
        max_v_amt = False
        max_v_pays = False
        if not payments['amount__sum']:
            # Compare value of policy max v. amount:
            max_v_amt = data['amount'] > policy_amt
        else:
            # Compare value of policy max v. payments + amount
            max_v_pays = payments['amount__sum'] + data['amount'] > policy_amt
        if max_v_amt or max_v_pays:
            # Amount on policy has been exceeded
            raise serializers.ValidationError('POLICY_AMOUNT_EXCEEDED')
        else:
            return data
