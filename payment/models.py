from django.db import models


class Payment(models.Model):
    ACCEPTED = 1
    DENIED = 0
    STAT_CHOICES = (
        (ACCEPTED, 'Accepted'),
        (DENIED, 'Denied'),
    )

    external_user_id = models.CharField(max_length=100)
    benefit = models.CharField(max_length=100)
    currency = models.CharField(max_length=3)
    amount = models.PositiveIntegerField()
    status = models.PositiveSmallIntegerField(choices=STAT_CHOICES)
    timestamp = models.DateTimeField(auto_now_add=True)
